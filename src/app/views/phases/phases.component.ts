import { TasksService } from "./../../services/tasks.service";
import { PhasesService } from "./../../services/phases.service";
import { Component, OnInit } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";

@Component({
  selector: "app-phases",
  templateUrl: "./phases.component.html",
  styleUrls: ["./phases.component.scss"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class PhasesComponent implements OnInit {
  UserRequest = {
    phase_id: "",
    project_id: localStorage.getItem("project_id"),
    role: localStorage.getItem("role"),
    data: {
      employee_name: localStorage.getItem("employee_id"),
      task_id: ""
    }
  };
  taskheadElements = [
    "#",
    "ID",
    "Name",
    "Description",
    "Status",
    "Priority",
    "Estimated Time",
    "Logged Time",
    "Options"
  ];
  submitted = false;

  newTask: any;
  tableData: any;
  PhaseData: Array<any>;
  currentPhaseData: any;
  selectedPhaseData: any;
  selectedTaskData: any;
  constructor(
    private formBuilder: FormBuilder,
    private phaseService: PhasesService,
    private taskService: TasksService
  ) {}

  get f() {
    return this.form.controls;
  }

  form: FormGroup;

  ngOnInit() {
    // Get all phases data
    this.phaseService.getAllPhase(this.UserRequest).subscribe((res: any) => {
      this.PhaseData = res.data;
    });
    // Get Current Phase Data
    this.phaseService
      .getCurrentPhase(this.UserRequest)
      .subscribe((res: any) => {
        this.currentPhaseData = res.data.data[0];
        console.log(this.currentPhaseData);
        this.UserRequest.phase_id = this.currentPhaseData.phase_id;
        this.UserRequest.project_id = this.currentPhaseData.project_id;
        console.log(this.UserRequest);
        this.taskService
          .getTasksTable(this.UserRequest)
          .subscribe((res: any) => {
            console.log(res);
            this.tableData = res.data;
          });
      });

    this.form = this.formBuilder.group({
      task_name: ["", [Validators.required]],
      description: ["", [Validators.required]],
      estimated_time: ["", [Validators.required]],
      priority: ["", [Validators.required]],
      status:[""]
    });
  }

  changePhase() {
    this.UserRequest.phase_id = this.selectedPhaseData;
    // Get Current Phase Data
    this.phaseService.getPhaseById(this.UserRequest).subscribe((res: any) => {
      this.currentPhaseData = res.data.data;
      console.log(this.currentPhaseData);
      this.UserRequest.phase_id = this.currentPhaseData.phase_id;
      this.UserRequest.project_id = this.currentPhaseData.project_id;
      console.log(this.UserRequest);
      this.taskService.getTasksTable(this.UserRequest).subscribe((res: any) => {
        this.tableData = res.data;
        console.log(res);
      });
    });
  }

  createTasks() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    this.newTask = {
      phase_id: this.currentPhaseData.phase_id,
      employee_id: "",
      task_name: this.form.get("task_name").value,
      description: this.form.get("description").value,
      estimated_time: this.form.get("estimated_time").value,
      status: "Available",
      priority: this.form.get("priority").value,
      project_id: localStorage.getItem("project_id")
    };
    this.UserRequest.data = this.newTask;
    console.log(this.UserRequest.data);
    this.taskService.createTask(this.UserRequest).subscribe((res: any) => {
      console.log(res);
      window.location.reload();
    });
  }
  getTaskId(id: any) {
    this.UserRequest.data.task_id = id;
    console.log(this.UserRequest);
  }

  getSelectedTaskData(task:any){
    this.selectedTaskData = task;
    console.log(this.selectedTaskData);
  }

  deleteTask() {
        console.log(this.UserRequest);
    this.taskService.deleteTask(this.UserRequest).subscribe((res: any) => {
      console.log(res);
      window.location.reload();
    });
  }

  updateTask(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    this.newTask = {
      phase_id: this.currentPhaseData.phase_id,
      employee_id: "",
      task_name: this.form.get("task_name").value,
      description: this.form.get("description").value,
      estimated_time: this.form.get("estimated_time").value,
      status: "Available",
      priority: this.form.get("priority").value,
      project_id: localStorage.getItem("project_id")
    };
    this.UserRequest.data = this.newTask;
    console.log(this.UserRequest.data);
    this.taskService.updateTask(this.UserRequest).subscribe((res: any) => {
      console.log(res);
      window.location.reload();
    });  }
}
