import { TasksService } from './../../services/tasks.service';
import { EmployeesService } from './../../services/employees.service';
import { Component, OnInit, ViewChild } from "@angular/core";
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: "app-employees",
  templateUrl: "./employees.component.html",
  styleUrls: ["./employees.component.scss"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class EmployeesComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource: any = [];
  isAssignedArray: any[] = [];
  isUnassignedArray: any[] = [];
  isAssignedValue: Boolean;
  isUnassignedValue: Boolean;
  employeeData: any;
  selectedEmployeeId: any;
  availableTasksData: any;
  headElements = ["#", "ID", "Name", "Role", "Option"];
  taskheadElements = [
    "#",
    "ID",
    "Name",
    "Description",
    "Status",
    "Priority",
    "Estimated Time"
  ];
  assignedTaskData: any;
  unassignedTaskData: any;
  UserRequest = {
    project_id: "projectPoseidon",
    phase_id: "005",
    employee_id: "",
    data: {}
  };

  selectedEmployeeTasksData: [];

  constructor(
    private employeesService: EmployeesService,
    private tasksService: TasksService
  ) {}

  ngOnInit() {
    // Get Employee Data and Available Tasks from Backend
    this.employeesService
      .getAllEmployees(this.UserRequest)
      .subscribe((res: any) => {
        this.employeeData = res.data;
        this.dataSource = new MatTableDataSource(res.data);
        console.log(this.employeeData);
      });
    this.tasksService
      .getAvailableTask(this.UserRequest)
      .subscribe((res: any) => {
        console.log(res);
        this.availableTasksData = res.data;
        for (let i = 0; i < res.data.length; i++) {
          this.isAssignedArray.push((this.isAssignedValue = false));
        }
        console.log(this.isAssignedArray);
      });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  // Get Selected Employee's ID
  getEmployeeIdOnClickButton(employee_id: String) {
    this.selectedEmployeeId = employee_id;
    console.log(this.selectedEmployeeId);
    this.getSelectedEmployeeTasks();
  }
  // Get Selected Employee's Tasks.
  getSelectedEmployeeTasks() {
    this.UserRequest.employee_id = this.selectedEmployeeId;
    this.tasksService
      .getTasksInPhaseByEmployeeId(this.UserRequest)
      .subscribe((res: any) => {
        console.log(res);
        this.selectedEmployeeTasksData = res.data;
        for (let i = 0; i < res.data.length; i++) {
          this.isUnassignedArray.push((this.isUnassignedValue = false));
        }
        console.log(this.isUnassignedArray);
      });
  }

  assigningTasks(task: any) {
    this.assignedTaskData = {
      phase_id: task.phase_id,
      task_id: task.task_id,
      employee_id: this.selectedEmployeeId,
      task_name: task.task_name,
      description: task.description,
      estimated_time: task.estimated_time,
      status: "Open",
      priority: task.priority,
      project_id: task.project_id
    };

    this.UserRequest.data = this.assignedTaskData;
    console.log(this.UserRequest.data);
    this.tasksService.updateTask(this.UserRequest).subscribe((res: any) => {
      console.log(res);
    });
  }
  hideButton(idx: any) {
    this.isAssignedArray[idx] = true;
  }
  closeAssignModal() {
    window.location.reload();
  }
  hideUnassignButton(idx: any) {
    this.isUnassignedArray[idx] = true;
  }
  //Unassign  task
  unassignTask(task: any) {
    this.unassignedTaskData = {
      phase_id: task.phase_id,
      task_id: task.task_id,
      employee_id: "",
      task_name: task.task_name,
      description: task.description,
      estimated_time: task.estimated_time,
      status: "Available",
      priority: task.priority,
      project_id: task.project_id
    };
    this.UserRequest.data = this.unassignedTaskData;
    console.log(this.UserRequest.data);
    this.tasksService.updateTask(this.UserRequest).subscribe((res: any) => {
      console.log(res);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}


