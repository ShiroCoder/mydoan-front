import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class TasksService {
  ENDPOINT_URL = "http://localhost:3000";

  constructor(public http: HttpClient, _router: Router) {}

  getTasksTable(UserRequest: any) {
    return this.http.post(`${this.ENDPOINT_URL}/phases/tasks/`, UserRequest);
  }
  getTasksInPhaseByEmployeeId(UserRequest: any) {
    return this.http.post(`${this.ENDPOINT_URL}/employees/getemployeetasks/`, UserRequest);
  }
  getAvailableTask(UserRequest: any) {
    return this.http.post(`${this.ENDPOINT_URL}/employees/getavailabletask/`,  UserRequest);
  }
  updateTask(UserRequest: any) {
    return this.http.post(`${this.ENDPOINT_URL}/employees/updatetasks/`, UserRequest);
  }
   createTask(UserRequest: any) {
    return this.http.post(`${this.ENDPOINT_URL}/phases/createtasks/`, UserRequest);
  }
  deleteTask(UserRequest: any) {
    return this.http.post(`${this.ENDPOINT_URL}/phases/deletetask/`, UserRequest);
  }
}