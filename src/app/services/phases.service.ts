import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhasesService {
  ENDPOINT_URL = 'http://localhost:3000';
  constructor(public http: HttpClient, _router: Router) {}

  getAllPhase(UserRequest: any) {
    return this.http.post(
      `${this.ENDPOINT_URL}/phases/getallphase/`,
      UserRequest
    );
  }
  getPhaseById(UserRequest: any) {
    return this.http.post(
      `${this.ENDPOINT_URL}/phases/getphasebyid/`,
      UserRequest
    );
  }
  getCurrentPhase(UserRequest: any) {
    return this.http.post(`${this.ENDPOINT_URL}/phases/currentphase/`, UserRequest);
  }
}
