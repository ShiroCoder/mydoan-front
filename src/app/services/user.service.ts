import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: "root"
})
export class UserService {
  ENDPOINT_URL = "http://localhost:3000";

  constructor(public http: HttpClient, _router: Router) {}
  
  login(user: any) {
    return this.http.post(`${this.ENDPOINT_URL}/login/`, user);
  }

  register(user: any) {
    return this.http.post(`${this.ENDPOINT_URL}/register`, user);
  }

}
