import { HrEmployeesComponent } from './views/hr-employees/hr-employees.component';
import { MyTaskComponent } from "./views/my-task/my-task.component";
import { MainComponent } from "./main-layout/main/main.component";
import { LoginComponent } from "./login/login.component";
import { MyProfileComponent } from "./views/my-profile/my-profile.component";
import { EmployeesComponent } from "./views/employees/employees.component";
import { TasksComponent } from "./views/tasks/tasks.component";
import { PhasesComponent } from "./views/phases/phases.component";
import { ModalsComponent } from "./views/modals/modals.component";
import { RouterModule, Route } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { NotFoundComponent } from "./views/errors/not-found/not-found.component";
import { Dashboard1Component } from "./views/dashboards/dashboard1/dashboard1.component";
const routes: Route[] = [
  { path: "", pathMatch: "full", redirectTo: "login" },

  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "",
    component: MainComponent,
    children: [
      {
        path: "phases",
        component: PhasesComponent
      },
      {
        path: "employees",
        component: EmployeesComponent
      },
      {
        path: "phases",
        component: TasksComponent
      },
      {
        path: "myprofile",
        component: MyProfileComponent
      },
      {
        path: "dashboards",
        children: [{ path: "v1", component: Dashboard1Component }]
      },
      {
        path: "mytasks",
        component: MyTaskComponent
      },
      {
        path: "hremployee",
        component:HrEmployeesComponent
      }
    ]
  },

  // {
  //   path: "phases",
  //   children: [{ path: "", component: PhasesComponent }]
  // },
  // {
  //   path: "employees",
  //   children: [{ path: "", component: EmployeesComponent }]
  // },
  // {
  //   path: "phases",
  //   children: [{ path: "tasks", component: TasksComponent }]
  // },
  // {
  //   path: "myprofile",
  //   children: [{ path: "", component: MyProfileComponent }]
  // },
  { path: "modals", component: ModalsComponent },
  { path: "**", component: NotFoundComponent }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
