import { UserService } from "./../services/user.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { first } from "rxjs/operators";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  user: any;
  error = false;
  messenger: string;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(7)]]
    });
  }

  get f() {
    return this.loginForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.user = {
      email: this.loginForm.get("email").value,
      password: this.loginForm.get("password").value
    };
    this.userService
      .login(this.user)
      .pipe(first())
      .subscribe( 
        data => {
          localStorage.setItem("email", data["email"]);
          localStorage.setItem("token", data["token"]);
          localStorage.setItem("employee_id", data["employee_id"]);
          localStorage.setItem("role", data["role"]);
          localStorage.setItem("project_id", data["project_id"]);
            this.router.navigate(["/phases"]);
          this.messenger = `Sign-in successfully!`;
        },
        error => {
          this.error = true;
          this.messenger = `Password or email is incorrect!`;
        }
      );
  }
}
