import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from "@angular/router";


@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent implements OnInit {
  @ViewChild("sidenav", { static: true }) sidenav: ElementRef;

  clicked: boolean;
  UserRequest = {
    role: localStorage.getItem("role"),
    data: {
      employee_name: localStorage.getItem("employee_id")
    }
  };
  constructor(private router: Router) {
    this.clicked = this.clicked === undefined ? false : true;
  }

  ngOnInit() {
    console.log(this.UserRequest);
  }

  setClicked(val: boolean): void {
    this.clicked = val;
  }

  logOut() {
    localStorage.removeItem("email");
    localStorage.removeItem("role");
    localStorage.removeItem("project_id");
    localStorage.removeItem("employee_id");
    localStorage.removeItem("token");
    this.router.navigate(["/login"]);
  }
}
